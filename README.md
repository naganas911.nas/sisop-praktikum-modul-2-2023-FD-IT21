# Analisis Cara Kerja
## Soal 2

### Source Code

```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <signal.h>

void create_folder(char *path) {
    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"mkdir", path, NULL};
        execv("/bin/mkdir", argv);
    }
    sleep(1);
}

void download_images(char *path) {
    int i;
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", t);

    for (i = 0; i < 15; i++) {
        now = time(NULL);
        t = localtime(&now);
        char filename[50];
        sprintf(filename, "%s/%s_%d.jpg", path, timestamp, i+1);

        char url[50];
        int size = (now % 1000) + 50;
        sprintf(url, "https://picsum.photos/%d", size);

        pid_t pid = fork();
        if (pid == 0) {
            char *argv[] = {"wget", "-q", "-O", filename, url, NULL};
            execv("/usr/bin/wget", argv);
        }
        sleep(5);
    }

    char zip_file[50];
    sprintf(zip_file, "%s.zip", path);
    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"zip", "-qr", zip_file, path, NULL};
        execv("/usr/bin/zip", argv);
    }

    int status;
    waitpid(pid, &status, 0);

    pid = fork();
    if (pid == 0) {
        char *argv[] = {"rm", "-rf", path, NULL};
        execv("/bin/rm", argv);
    }
}

void create_killer(char *mode, char *path) {
    char killer_path[50];
    sprintf(killer_path, "%s/killer.sh", path);

    FILE *fptr = fopen(killer_path, "w");
    if (strcmp(mode, "-a") == 0) {
        fprintf(fptr, "#!/bin/bash\nkillall -SIGINT main\nrm -rf %s", path);
    } else if (strcmp(mode, "-b") == 0) {
        fprintf(fptr, "#!/bin/bash\nkillall -SIGTERM main\nrm %s/killer.sh", path);
    }
    fclose(fptr);

    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"chmod", "+x", killer_path, NULL};
        execvp("/bin/chmod", argv);
    }
    wait(NULL);

    pid = fork();
    if (pid == 0) {
        char *argv[] = {"./killer.sh", NULL};
        execvp("./killer.sh", argv);
    }
}

void handle_sigterm(int sig) {
    printf("Exiting...\n");
    exit(0);
}

int main(int argc, char *argv[]) {
    char mode[3];
    if (argc == 2) {
        strcpy(mode, argv[1]);
    } else {
        printf("Usage: ./main [-a/-b]\n");
        exit(0);
    }

    if (strcmp(mode, "-a") != 0 && strcmp(mode, "-b") != 0) {
        printf("Invalid argument. Usage: ./main [-a/-b]\n");
        exit(0);
    }
signal(SIGTERM, handle_sigterm);

while (1) {
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char foldername[20];
    strftime(foldername, sizeof(foldername), "%Y-%m-%d_%H:%M:%S", t);

    create_folder(foldername);
    download_images(foldername);
    create_killer(mode, foldername);

    sleep(30);
}

return 0;
}
```

### Penjelasan
- `create_folder()` - fungsi ini membuat folder baru dengan nama yang diberikan melalui parameter `path` menggunakan perintah `mkdir`. Proses ini dilakukan dengan membuat fork dan menjalankan perintah tersebut pada child process.
- `download_images()` - fungsi ini mengunduh beberapa gambar dari situs web menggunakan perintah `wget`. Untuk setiap gambar yang diunduh, program menunggu selama 5 detik sebelum melanjutkan ke gambar berikutnya. Setelah mengunduh semua gambar, fungsi ini memampatkan folder menggunakan perintah `zip`. Setelah proses pemampatan selesai, fungsi ini menghapus folder yang dibuat menggunakan perintah `rm`.
- `create_killer()` - fungsi ini membuat file shell script yang akan membunuh program ini atau dirinya sendiri dengan menggunakan perintah `killall`. Mode pemilihan yang digunakan di antara "-a" dan "-b" menentukan sinyal apa yang digunakan untuk membunuh program ini. Setelah membuat file shell script, fungsi ini memberikan izin akses eksekusi pada file tersebut menggunakan perintah `chmod` dan menjalankannya menggunakan `execvp`.
- `handle_sigterm()` - fungsi ini menangani sinyal SIGTERM dan mencetak pesan keluar dari program sebelum keluar.
- `main()` - fungsi utama program. Program menerima argumen mode "-a" atau "-b" yang menentukan sinyal apa yang digunakan untuk membunuh program. Setelah memeriksa argumen, program akan memulai loop utama yang memanggil fungsi `create_folder()`, `download_images()`, dan `create_killer()` dan menunggu selama 30 detik sebelum memulai iterasi berikutnya. Fungsi `handle_sigterm()` juga disetel untuk menangani sinyal SIGTERM.
- `signal()` - fungsi ini digunakan untuk menetapkan penangan sinyal untuk program. Di sini, `handle_sigterm()` ditetapkan sebagai penangan sinyal untuk SIGTERM.
- Pada fungsi `download_images()`, terdapat dua variabel `now` dan `t` yang digunakan untuk mendapatkan waktu saat ini dan mengubahnya menjadi format waktu yang sesuai menggunakan fungsi `localtime()` dan `strftime()`. Variabel `timestamp` digunakan untuk menyimpan format waktu dalam string.
- Selain itu, variabel `filename` digunakan untuk menyimpan nama file gambar yang diunduh. Nama file terdiri dari path folder, timestamp, dan nomor gambar yang diunduh.
- Variabel `url` digunakan untuk menyimpan URL dari gambar yang akan diunduh. Ukuran gambar yang diunduh dihitung dari variabel `size`, yang dihasilkan dari operasi modulus pada `now` dan ditambah dengan 50.
- Pada fungsi `create_killer()`, terdapat file shell script yang dibuat dengan nama `killer.sh`. File ini akan digunakan untuk membunuh program atau dirinya sendiri. Pemilihan mode "-a" akan membunuh program dengan sinyal SIGINT, sedangkan mode "-b" akan membunuh program dengan sinyal SIGTERM.
- Setelah file `killer.sh` dibuat, izin akses eksekusi diberikan pada file tersebut menggunakan perintah `chmod`. Kemudian, file tersebut dijalankan menggunakan `execvp()`.
- Di dalam loop utama pada fungsi `main()`, fungsi `create_folder()`, `download_images()`, dan `create_killer()` dipanggil secara berurutan dengan argumen `foldername` yang merupakan format waktu saat ini.
- Setelah semua proses dijalankan, program akan menunggu selama 30 detik sebelum memulai iterasi berikutnya. Selama menunggu, program dapat menerima sinyal SIGTERM yang akan ditangani oleh fungsi `handle_sigterm()`.
- Pada bagian awal dari fungsi `main()`, program memeriksa jumlah argumen yang diterima dari baris perintah. Jika argumen yang diterima tidak sesuai dengan yang diharapkan, program akan mencetak pesan kesalahan dan keluar dari program. 
- Kemudian, program memeriksa argumen yang diberikan dan memastikan bahwa hanya mode "-a" atau "-b" yang diterima. Jika argumen tidak valid, program akan mencetak pesan kesalahan dan keluar dari program.
- Pada bagian akhir dari fungsi `main()`, program akan kembali ke awal loop dan memanggil kembali fungsi-fungsi yang diperlukan. Selain itu, program akan menunggu selama 30 detik sebelum memulai iterasi berikutnya.

## SOAL 4

### Source Code 

```
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

void handle_sigint(int signal);
void run_script(char* path_to_script);
void print_usage();
void print_error();
int is_valid_time(int hour, int minute, int second);
int parse_time(char* arg, int* dest);
void cron_job(int hour, int minute, int second, char* path_to_script);

int main(int argc, char* argv[]) {

    // Set up signal handler for SIGINT
    signal(SIGINT, handle_sigint);

    // Validate and parse arguments
    if (argc != 5) {
        print_usage();
        return 1;
    }
    int hour, minute, second;
    if (!parse_time(argv[1], &hour) ||
        !parse_time(argv[2], &minute) ||
        !parse_time(argv[3], &second) ||
        !is_valid_time(hour, minute, second)) {
        print_error();
        return 1;
    }
    char* path_to_script = argv[4];

    // Run cron job
    cron_job(hour, minute, second, path_to_script);

    return 0;
}

void handle_sigint(int signal) {
    printf("Cron job terminated.\n");
    exit(0);
}

void run_script(char* path_to_script) {
    int pid = fork();
    if (pid == 0) {
        execlp(path_to_script, path_to_script, NULL);
        exit(1);
    }
}

void print_usage() {
    printf("Usage: cron <hour> <minute> <second> <path_to_script>\n");
}

void print_error() {
    printf("Error: Invalid arguments.\n");
    print_usage();
}

int is_valid_time(int hour, int minute, int second) {
    return hour >= 0 && hour <= 23 &&
           minute >= 0 && minute <= 59 &&
           second >= 0 && second <= 59;
}

int parse_time(char* arg, int* dest) {
    char* endptr;
    int val = strtol(arg, &endptr, 10);
    if (*endptr != '\0' || val < 0) {
        return 0;
    }
    *dest = val;
    return 1;
}

void cron_job(int hour, int minute, int second, char* path_to_script) {
    while (1) {
        time_t current_time = time(NULL);
        struct tm* timeinfo = localtime(&current_time);
        if (timeinfo->tm_hour == hour &&
            timeinfo->tm_min == minute &&
            timeinfo->tm_sec == second) {
            run_script(path_to_script);
        }
        sleep(1);
    }
}
```

### Penjelasan
- `handle_sigint()` - fungsi ini menangani sinyal SIGINT dan mencetak pesan sebelum keluar dari program. 
- `run_script()` - fungsi ini menjalankan script yang diberikan melalui parameter `path_to_script` menggunakan perintah `execlp`. Proses ini dilakukan dengan membuat fork dan menjalankan perintah tersebut pada child process. 
- `print_usage()` dan `print_error()` - fungsi ini mencetak pesan tentang penggunaan yang benar dan pesan kesalahan jika terjadi kesalahan argumen. 
- `is_valid_time()` - fungsi ini memeriksa apakah waktu yang diberikan dalam bentuk jam, menit, dan detik adalah waktu yang valid. 
- `parse_time()` - fungsi ini mengubah argumen waktu yang diberikan dalam bentuk string menjadi integer. Fungsi ini juga memeriksa apakah argumen waktu yang diberikan valid. 
- `cron_job()` - fungsi ini memeriksa waktu saat ini setiap detik dan menjalankan script jika waktu yang telah ditentukan telah tercapai. Fungsi ini dijalankan terus-menerus hingga program diberhentikan. 
- Pada fungsi `main()`, program memeriksa jumlah argumen yang diterima dari baris perintah. Jika argumen yang diterima tidak sesuai dengan yang diharapkan, program akan mencetak pesan kesalahan dan keluar dari program.
- Kemudian, program memeriksa argumen waktu yang diberikan dan memastikan bahwa waktu yang diberikan valid. Jika argumen tidak valid, program akan mencetak pesan kesalahan dan keluar dari program.
- Setelah memeriksa argumen, program memanggil fungsi `cron_job()` dan menjalankan cron job.
- Pada bagian awal dari program, program mengatur penangan sinyal SIGINT menggunakan fungsi `signal()`. Hal ini diperlukan agar program dapat menangani sinyal yang diberikan oleh pengguna saat program berjalan. 
- Program juga menggunakan perintah `fork()` untuk membuat proses baru saat program menjalankan script. Proses baru ini dibuat untuk menjalankan script pada child process dan menghindari mengganggu program utama. 
- Program ini juga menggunakan fungsi `sleep()` untuk memberikan jeda waktu antara dua operasi yang berbeda, seperti jeda waktu antara pengecekan waktu saat ini dan jalannya script. 
- Pada bagian akhir dari program, fungsi `return 0`; digunakan untuk menandakan bahwa program telah berakhir dengan sukses. Hal ini tidak terlalu diperlukan karena program ini tidak mengembalikan nilai apa pun, namun hal ini umum dilakukan pada program C sebagai tanda bahwa program telah selesai dieksekusi dengan benar.
