#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

void handle_sigint(int signal);
void run_script(char* path_to_script);
void print_usage();
void print_error();
int is_valid_time(int hour, int minute, int second);
int parse_time(char* arg, int* dest);
void cron_job(int hour, int minute, int second, char* path_to_script);

int main(int argc, char* argv[]) {

    // Set up signal handler for SIGINT
    signal(SIGINT, handle_sigint);

    // Validate and parse arguments
    if (argc != 5) {
        print_usage();
        return 1;
    }
    int hour, minute, second;
    if (!parse_time(argv[1], &hour) ||
        !parse_time(argv[2], &minute) ||
        !parse_time(argv[3], &second) ||
        !is_valid_time(hour, minute, second)) {
        print_error();
        return 1;
    }
    char* path_to_script = argv[4];

    // Run cron job
    cron_job(hour, minute, second, path_to_script);

    return 0;
}

void handle_sigint(int signal) {
    printf("Cron job terminated.\n");
    exit(0);
}

void run_script(char* path_to_script) {
    int pid = fork();
    if (pid == 0) {
        execlp(path_to_script, path_to_script, NULL);
        exit(1);
    }
}

void print_usage() {
    printf("Usage: cron <hour> <minute> <second> <path_to_script>\n");
}

void print_error() {
    printf("Error: Invalid arguments.\n");
    print_usage();
}

int is_valid_time(int hour, int minute, int second) {
    return hour >= 0 && hour <= 23 &&
           minute >= 0 && minute <= 59 &&
           second >= 0 && second <= 59;
}

int parse_time(char* arg, int* dest) {
    char* endptr;
    int val = strtol(arg, &endptr, 10);
    if (*endptr != '\0' || val < 0) {
        return 0;
    }
    *dest = val;
    return 1;
}

void cron_job(int hour, int minute, int second, char* path_to_script) {
    while (1) {
        time_t current_time = time(NULL);
        struct tm* timeinfo = localtime(&current_time);
        if (timeinfo->tm_hour == hour &&
            timeinfo->tm_min == minute &&
            timeinfo->tm_sec == second) {
            run_script(path_to_script);
        }
        sleep(1);
    }
}
